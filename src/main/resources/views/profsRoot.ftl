<#ftl encoding="utf-8">


<body xmlns="http://www.w3.org/1999/html">

<ul>
    <h1> Liste des enseignants: </h1>
    <#list profs as prof>
        <li>${prof.id} - ${prof.firstName} ${prof.lastName} aka ${prof.userName} </li>
    </#list>
</ul>


<form method="post" action="profs/delete">
    <label for="id">id</label>
    <input name="id" required>
    <button type="submit" placeholder="Supprimer...">Envoyer</button>
</form>

</body>

</html>
