<#ftl encoding="utf-8">


<body xmlns="http://www.w3.org/1999/html">

<ul>
    <h1> Liste des enseignants: </h1>
    <#list profs as prof>
        <li>${prof.id} - ${prof.firstName} ${prof.lastName} aka ${prof.userName} </li>
    </#list>
</ul>

<a href="/welcome">Retour à la page d'accueil</a>

</body>

</html>
