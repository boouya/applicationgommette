<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <h1> Gommettes de l'élève ${eleve.firstName} ${eleve.lastName} (classe : ${eleve.nomClasse}) </h1>

    <#list attributions as attrib>
        <h2>
            <li>${attrib.gommette.couleur.format(attrib.gommette.nom)} :</li>
            <h3>
                <p> description: ${attrib.gommette.description} </p>
                <p> raison: ${attrib.gommetteAttrib.raisonAttribution} </p>
                <p> le ${attrib.gommetteAttrib.dateAttribution} par ${attrib.prof.firstName} ${attrib.prof.lastName} </p>
            </h3>

            <form action="/eleves/gommette/delete" method="post">
            <input  type="hidden" name="idEleve" value="${eleve.id}">
            <input  type="hidden" name="id" value="${attrib.gommetteAttrib.id}">
            <button type="submit" name="id">Supprimer</button>

        </form>
        </h2>
    </#list>


<h2>Ajouter une gommette</h2>
<form action="/eleves/gommette/ajout" method="post">
    <input  type="hidden" name="idEleve" value="${eleve.id}">

    <label for="idGommette">Id de la gommette</label>
    <input name="idGommette" type="text">

    <label for="raison">Raison</label>
    <input name="raison" type="text" required>

    <button type="submit">Ajouter</button>
</form>

</ul>

<footer>
<a href="/welcome">Accueil</a>
<a href="/profs" >Liste des profs</a>
<a href="/eleves" >Liste des eleves</a>
<a href="/gommettes">Liste des gommettes</a>
</footer>

</body>
</html>
