<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <h1> Liste des utilisateurs: </h1>
    <#list users as user>
        <li>${user.id} - ${user.firstName} ${user.lastName} </li>
    </#list>
</ul>

</body>

</html>
