<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <#list gommetteAttribs as gommetteAttrib>
        <li>${gommetteAttrib.id} - eleve:${gommetteAttrib.idEleve}, prof:${gommetteAttrib.idProf}, raison:${gommetteAttrib.raison} </li>
    </#list>
</ul>

</body>

</html>
