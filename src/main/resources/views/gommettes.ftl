<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <h1> Liste des gommettes: </h1>

    <#list gommettes as gommette>
        <li>${gommette.id} - ${gommette.couleur.format(gommette.nom)} : ${gommette.description} </li>
    </#list>
</ul>



<h2>Ajouter une gommette</h2>
<form action="/gommettes/ajout" method="post">

    <label for="nom">Nom</label>
    <input type="text" name="nom" required>


    <input type="radio" id="choixArcEnCiel" name="couleur" value="arcEnCiel">
    <label for="choixArcEnCiel">Arc en ciel</label>

    <input type="radio" id="choixRouge" name="couleur" value="rouge">
    <label for="choixRouge">Rouge</label>

    <input type="radio" id="choixVert" name="couleur" value="vert">
    <label for="choixVert">Vert</label>

    <input type="radio" id="choixBleu" name="couleur" value="blue">
    <label for="choixBleu">Bleu</label>

    <input type="radio" id="choixBlanc" name="couleur" value="blanc">
    <label for="choixBlanc">Blanc</label>

    <br>
    <label for="description">Description</label>
    <input type="text" name="description" required>

    <br>
    <button type="submit">Creer</button>
</form>



<h1>Modifier une gommette</h1>

<form action="/gommettes/update" method="post" id="update_form">

    <label for="nom">Nom</label>
    <input type="text" name="nom">


    <input type="radio" id="choixArcEnCiel" name="couleur" value="arcEnCiel">
    <label for="choixArcEnCiel">Arc en ciel</label>

    <input type="radio" id="choixRouge" name="couleur" value="rouge">
    <label for="choixRouge">Rouge</label>

    <input type="radio" id="choixVert" name="couleur" value="vert">
    <label for="choixVert">Vert</label>

    <input type="radio" id="choixBleu" name="couleur" value="blue">
    <label for="choixBleu">Bleu</label>

    <input type="radio" id="choixBlanc" name="couleur" value="blanc">
    <label for="choixBlanc">Blanc</label>

    <br>
    <label for="description">Description</label>
    <input type="text" name="description">

    <br>
    <button type="submit">Modifier</button>
</form>

<label for="id_selection">Selectionner l'identifiant</label>
<select name="id_selection" form="update_form">
    <#list gommettes as gommette>
        <option value="${gommette.id}">${gommette.id}</option>
    </#list>
</select>

<footer>
<a href="/welcome">Accueil</a>
<a href="/profs" >Liste des profs</a>
<a href="/eleves" >Liste des eleves</a>
<a href="/gommettes">Liste des gommettes</a>
</footer>
    
</body>

</html>
