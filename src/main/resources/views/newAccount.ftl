<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">


<form method="post" action="/createaccount">
    <label for="firstname">Prénom</label>
    <input name="firstname" required>

    <label for="lastname">Nom de famille</label>
    <input name="lastname" required>

    <label for="userName">Nom d'utilisateur</label>
    <input name="userName" required>

    <label for="password">Mot de passe</label>
    <input name="password" type="password" required>

    <button type="submit" placeholder="Créer un compte">Envoyer</button>
</form>


<a href="/login">Retour</a>

</body>

</html>
