<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <h1> Liste des élèves: </h1>
    <#list eleves as eleve>

        <li><a href="eleves/${eleve.id}">${eleve.id} - ${eleve.nomClasse} - ${eleve.firstName} ${eleve.lastName} (${eleve.dateNaissance})</a> </li>

        <form action="eleves/delete" method="post">
            <input  type="hidden" name="id" value="${eleve.id}">
            <button type="submit" name="id">Supprimer</button>
        </form>
    </#list>

</ul>



<h2>Ajouter un élève</h2>
<form action="/eleves/ajout" method="post">

    <label for="firstName">Prenom</label>
    <input name="firstName" type="text" required>

    <label for="lastName">Nom</label>
    <input name="lastName" type="text">

    <label for="dateNaissance">Date de Naissance</label>
    <input name="dateNaissance" type="date" required>

    <label for="nomClasse">Nom de classe</label>
    <input name="nomClasse" type="text" required>

    <button type="submit">Creer</button>
</form>


<h1>Modifier un élève</h1>

<form action="/eleves/update" method="post" id="update_form">

    <label for="firstName">Prenom</label>
    <input name="firstName" type="text">

    <label for="lastName">Nom</label>
    <input name="lastName" type="text">

    <label for="dateNaissance">Date de Naissance</label>
    <input name="dateNaissance" type="date">

    <label for="nomClasse">Nom de classe</label>
    <input name="nomClasse" type="text">

    <button type="submit">Modifier</button>
</form>

<label for="id_selection">Selectionner l'identifiant</label>

<select name="id_selection" form="update_form">
    <#list eleves as eleve>
        <option value="${eleve.id}">${eleve.id}</option>
    </#list>
</select>
    

<footer>
<a href="/welcome">Accueil</a>
<a href="/profs" >Liste des profs</a>
<a href="/eleves" >Liste des eleves</a>
<a href="/gommettes">Liste des gommettes</a>
</footer>

</body>

</html>
