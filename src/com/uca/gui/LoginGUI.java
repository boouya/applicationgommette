package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;

public class LoginGUI 
{
    public static String getConnectionPage() throws IOException, TemplateException {

        Map<String, Object> input = new HashMap<>();
        return DefaultGUI.getDefaultGUI(input, "login.ftl");
    }
}