package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;
import com.uca.entity.*;

public class EleveGommetteGUI 
{
    public static String getEleveGommettes(int id) throws IOException, TemplateException {

        Map<String, Object> input = new HashMap<>();
        input.put("eleve", Core.Eleve.getById(id));

        List<GommetteAttribEntity> gommettesAttribs = Core.GommetteAttrib.getAll();
        for(int i = gommettesAttribs.size()-1; i >= 0; i--)
        {
            if(gommettesAttribs.get(i).getIdEleve() != id)
            {
                gommettesAttribs.remove(i);
            }
        }

        List<AttributionInfo> attributions = new ArrayList<>();
        for (GommetteAttribEntity g : gommettesAttribs) {
            attributions.add(new AttributionInfo(g, Core.Gommette.getById(g.getIdGommette()), Core.Prof.getById(g.getIdProf())));
        }

        input.put("attributions", attributions);
        return DefaultGUI.getDefaultGUI(input, "eleveGommettes.ftl");
    }
}
