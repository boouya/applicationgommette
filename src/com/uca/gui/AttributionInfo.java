package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;
import com.uca.entity.*;

public class AttributionInfo 
{
    private GommetteAttribEntity gommetteAttrib;
    public  GommetteAttribEntity getGommetteAttrib() { return gommetteAttrib; } 
    public  void setGommetteAttrib(GommetteAttribEntity value) { gommetteAttrib = value; }

    private GommetteEntity gommette;
    public  GommetteEntity getGommette() { return gommette; } 
    public  void setGommetteAttrib(GommetteEntity value) { gommette = value; }

    private ProfEntity prof;
    public  ProfEntity getProf() { return prof; } 
    public  void setProf(ProfEntity value) { prof = value; }

    public AttributionInfo(GommetteAttribEntity gommetteAttrib, GommetteEntity gommette, ProfEntity prof)
    {
        this.gommetteAttrib = gommetteAttrib;
        this.gommette = gommette;
        this.prof = prof;
    }
}