package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;

public class UserGUI 
{
    public static String getAllUsers() throws IOException, TemplateException {

        Map<String, Object> input = new HashMap<>();
        input.put("users", Core.User.getAll());
        return DefaultGUI.getDefaultGUI(input, "users.ftl");
    }    
}