package com.uca.gui;

import com.uca.core.UserCore;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

public class DefaultGUI 
{
    public static String getDefaultGUI(Map<String, Object> input, String templateName)
    {
        try
        {
            Configuration configuration = _FreeMarkerInitializer.getContext();
            Writer output = new StringWriter();
            Template template = configuration.getTemplate(templateName);
            template.setOutputEncoding("UTF-8");
            template.process(input, output);
            return output.toString();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return "error when generating the default gui. templateName: "+templateName;
    }
}
