package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;
import com.uca.entity.*;
import com.uca.gui.*;

public class GommetteGUI 
{
    public static String getAllGommettes() throws IOException, TemplateException {

        Map<String, Object> input = new HashMap<>();
        input.put("gommettes", Core.Gommette.getAll());

        return DefaultGUI.getDefaultGUI(input, "gommettes.ftl");
    }
}
