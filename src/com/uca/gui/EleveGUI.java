package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;
import  com.uca.entity.*;

public class EleveGUI 
{
    public static String getAllEleves() throws IOException, TemplateException {

        Map<String, Object> input = new HashMap<>();
        input.put("eleves", Core.Eleve.getAll());
        return DefaultGUI.getDefaultGUI(input, "eleves.ftl");
    }
}
