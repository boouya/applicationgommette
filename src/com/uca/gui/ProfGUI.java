package com.uca.gui;

import com.uca.core.*;
import freemarker.template.*;
import java.io.*;
import java.util.*;
import  com.uca.entity.*;

public class ProfGUI 
{
    public static String getAllProfs() throws IOException, TemplateException {

        Map<String, Object> input = new HashMap<>();
        input.put("profs", Core.Prof.getAll());
        return DefaultGUI.getDefaultGUI(input, "profsRegular.ftl");
    }


    public static String getWelcomePage(ProfEntity prof)
    { 
        Map<String, Object> input = new HashMap<>();
        input.put("prof",prof);
        return DefaultGUI.getDefaultGUI(input, "welcome.ftl");
    }

}
