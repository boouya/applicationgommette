package src.com.uca.helper;

import java.security.MessageDigest;

public class Hash {
 
    public  static  String hashSHA256(String s)  throws Exception 
    {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(s.getBytes());

        byte byteData[] = md.digest();

        //convertir le tableau de bits en une format hexadécimal - méthode 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();

    }

}
