package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;
import java.sql.Date;

public class GommetteAttribCore extends _DefaultCore<GommetteAttribEntity> {

   public static final GommetteAttribCore Instance = new GommetteAttribCore();
   private GommetteAttribCore()
   {
      super(new GommetteAttribDAO());
   }

   public GommetteAttribEntity create(int idEleve, int idProf, int idGommette, java.sql.Date dateAttribution, String raison) {
      GommetteAttribEntity entity = new GommetteAttribEntity();
      entity.setIdEleve(idEleve);
      entity.setIdProf(idProf);
      entity.setIdGommette(idGommette);
      entity.setDateAttribution(dateAttribution);
      entity.setRaisonAttribution(raison);
      return dao.create(entity);
   }
}