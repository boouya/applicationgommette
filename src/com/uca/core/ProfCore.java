package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;
import src.com.uca.helper.Hash;
import com.uca.*;

public class ProfCore extends _DefaultCore<ProfEntity> {

    public static final ProfCore Instance = new ProfCore();

    private ProfCore()
    {
        super(new ProfDAO());
    }

    public ProfEntity create(String firstName, String lastName, String userName, String password) throws Exception {
        ProfEntity entity = new ProfEntity();
        entity.setFirstName(firstName);
        entity.setLastName (lastName);
        entity.setUserName (userName);
        entity.setPassword(Hash.hashSHA256(StartServer.SEL + password));
        return dao.create(entity);
    }


 
    public ProfEntity getByLoginPair(String username, String password) throws Exception 
    {
        String hash_pwd = Hash.hashSHA256(StartServer.SEL + password);
        for(ProfEntity p : dao.getAll())
        {
           
            if (p.getUserName().compareTo(username) == 0 && p.getPassword().compareTo(hash_pwd) == 0)
            {
                return p;
            }
        }
        return null;
    }
}
