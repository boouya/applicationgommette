package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;
import java.sql.Date;

public class EleveCore extends _DefaultCore<EleveEntity> {

    public static final EleveCore Instance = new EleveCore();
    private EleveCore()
    {
        super(new EleveDAO());
    }

    public EleveEntity create(String firstName, String lastName, java.sql.Date dateNaissance, String nomDeClasse) {
        EleveEntity entity = new EleveEntity();
        entity.setFirstName(firstName);
        entity.setLastName (lastName);
        entity.setDateNaissance(dateNaissance);
        entity.setNomClasse(nomDeClasse);
        return dao.create(entity);
    }

    public void update(int id,String firstName, String lastName, java.sql.Date dateNaissance, String nomDeClasse)
    {
        EleveEntity entity = new EleveEntity();

        EleveEntity previous = dao.getById(id);

        if (firstName.length() > 0)
        {
            entity.setFirstName(firstName);
        }
            
        else{
            entity.setFirstName(previous.getFirstName());
        }
            
        if (lastName.length() > 0){
            entity.setLastName (lastName);
        }
        else
        {
            entity.setLastName(previous.getLastName());
        }
        
        if (dateNaissance != null)
        {
            entity.setDateNaissance(dateNaissance);
            
        }
        else
        {
            entity.setDateNaissance(previous.getDateNaissance());
        }
        

        if (nomDeClasse.length() > 0)
        {
            entity.setNomClasse(nomDeClasse);
        }
        else
        {
            entity.setNomClasse(previous.getNomClasse());
            
        }
        
        entity.setId(id);

        dao.push(entity);
    }
}