package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;


public class GommetteCore extends _DefaultCore<GommetteEntity> {

    public static final GommetteCore Instance = new GommetteCore();

    private GommetteCore()
    {
        super(new GommetteDAO());
    }

    public GommetteEntity create(String nom, Couleur couleur, String description) {
        GommetteEntity entity = new GommetteEntity();
        entity.setNom(nom);
        entity.setCouleur(couleur);
        entity.setDescription(description);
        return dao.create(entity);
    }
}