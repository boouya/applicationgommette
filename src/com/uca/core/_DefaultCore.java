package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;

public abstract class _DefaultCore<T extends Entity> {
    
    protected final _DefaultDAO<T> dao;

    public _DefaultCore(_DefaultDAO<T> dao)
    {
        this.dao = dao;
    }

    public List<T> getAll() { return dao.getAll(); }

    public T push(T obj) { return dao.push(obj); }
    public void delete(T obj) { dao.delete(obj); }
    public void delete(int id) { dao.delete(id); }
    public T getById(int id) {return dao.getById(id); }
}
