package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;

public final class Core {

    private Core() { super(); }

    public static final UserCore     User  = UserCore.Instance;
    public static final EleveCore    Eleve = EleveCore.Instance;
    public static final ProfCore     Prof  = ProfCore.Instance;

    public static final GommetteCore       Gommette       = GommetteCore.Instance;
    public static final GommetteAttribCore GommetteAttrib = GommetteAttribCore.Instance;
}
