package com.uca.core;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;


public class UserCore extends _DefaultCore<UserEntity> {

    public static final UserCore Instance = new UserCore();
    
    private UserCore()
    {
        super(new UserDAO());
    }

    public UserEntity create(String firstName, String lastName) {
        UserEntity entity = new UserEntity();
        entity.setFirstName(firstName);
        entity.setLastName(lastName);
        return dao.create(entity);
    }
}
