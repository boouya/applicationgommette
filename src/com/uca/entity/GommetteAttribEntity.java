package com.uca.entity;
import java.sql.Timestamp;
import java.sql.Date;
import com.uca.entity.Entity;

public class GommetteAttribEntity extends Entity {
    private int  idEleve;
    public  int  getIdEleve() { return idEleve; }
    public  void setIdEleve(int nIdEleve) { idEleve = nIdEleve; }

    private int  idProf;
    public  int  getIdProf() { return idProf; }
    public  void setIdProf(int nIdProf) { idProf = nIdProf; }

    private int  idGommette;
    public  int  getIdGommette() { return idGommette; }
    public  void setIdGommette(int nIdGommette) { idGommette = nIdGommette; }

    private java.sql.Date dateAttribution;
    public  java.sql.Date getDateAttribution() { return dateAttribution; }
    public  void   setDateAttribution(java.sql.Date nDateAttribution) { dateAttribution = nDateAttribution; }

    private String raisonAttribution;
    public  String getRaisonAttribution() { return raisonAttribution; }
    public  void   setRaisonAttribution(String nRaisonAttribution) { raisonAttribution = nRaisonAttribution; }
}
