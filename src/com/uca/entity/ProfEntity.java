package com.uca.entity;
import java.sql.Timestamp;
import com.uca.entity.UserEntity;

public class ProfEntity extends UserEntity {
    private String UserName;
    private String password;
    
    public  String getUserName() { return UserName; } 
    public  void   setUserName(String value) { UserName = value; } 
    public void setPassword(String value) { password = value; }
    public String getPassword() { return password; }

    public ProfEntity() { }
}
