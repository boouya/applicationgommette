package com.uca.entity;
import java.sql.Timestamp;
import com.uca.entity.*;

public abstract class Entity  {

    private int id = -1;
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public Entity() {}
}
