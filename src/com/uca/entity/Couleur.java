package com.uca.entity;

import java.util.*;

public enum Couleur
{
    ArcEnCiel("arcEnCiel", null),
    Rouge("rouge", "FF0000"),
    Vert ("vert", "00FF00"),
    Bleu ("bleu", "0000FF"),
    Blanc("blanc", "FFFFFF");

    private String value;
    private String codeHexa;

    private Couleur(String value, String codeHexa)
    {
        this.value = value;
        this.codeHexa = codeHexa;
    }

    public String getString() { return value; }

    public static Couleur retrieve(String value)
    {
        if (value != null)
        {
            if (value.compareTo("rouge") == 0)
            {
                return Rouge;
            }
            else if (value.compareTo("blanc") == 0)
            { return Blanc; }
            else if (value == "bleu")
            {
                return Bleu;
            }
            else if (value.compareTo("vert") == 0)
            {
                return Vert;
            }
            else if (value.compareTo("arcEnCiel") == 0)
            {
                return ArcEnCiel;
            }
            else {
                return null;
            }
        }
        return null;
    }

    private static String formatWithColor(String txt, String codeHexa)
    {
        return "<font color=\"#"+codeHexa+"\">"+txt+"</font>";
    }

    public String format(String txt)
    {
        if (codeHexa != null)
        {
            return formatWithColor(txt, codeHexa);
        }
        
        String s = "";
        for(int i = 0; i < txt.length(); i++)
        {
            s += formatWithColor(txt.charAt(i)+"", hsvToRgb(i*27/360f, 1, 1));
        }
        return s;
    }

    // Thank to https://stackoverflow.com/questions/7896280/converting-from-hsv-hsb-in-java-to-rgb-without-using-java-awt-color-disallowe
    private static String hsvToRgb(float hue, float saturation, float value) {
        float r, g, b;
    
        int h = (int)(hue * 6);
        float f = hue * 6 - h;
        float p = value * (1 - saturation);
        float q = value * (1 - f * saturation);
        float t = value * (1 - (1 - f) * saturation);
    
        if (h == 0) {
            r = value;
            g = t;
            b = p;
        } else if (h == 1) {
            r = q;
            g = value;
            b = p;
        } else if (h == 2) {
            r = p;
            g = value;
            b = t;
        } else if (h == 3) {
            r = p;
            g = q;
            b = value;
        } else if (h == 4) {
            r = t;
            g = p;
            b = value;
        } else if (h <= 6) {
            r = value;
            g = p;
            b = q;
        } else {
            throw new RuntimeException("Something went wrong when converting from HSV to RGB. Input was " + hue + ", " + saturation + ", " + value);
        }
    
        String rs = Integer.toHexString((int)(r * 255));
        String gs = Integer.toHexString((int)(g * 255));
        String bs = Integer.toHexString((int)(b * 255));
        return rs + gs + bs;
    }
}   
