package com.uca.entity;
import java.sql.Timestamp;
import com.uca.entity.*;
import java.sql.Date;

public class EleveEntity extends UserEntity {

    private java.sql.Date dateNaissance;

    public  java.sql.Date getDateNaissance() { return dateNaissance; } 
    public  void   setDateNaissance(java.sql.Date value) { dateNaissance = value; }

    private String nomClasse;
    public  String getNomClasse() { return nomClasse; } 
    public  void   setNomClasse(String value) { nomClasse = value; } 

    public EleveEntity(){
        //Ignored !
    }
}
