package com.uca.entity;
import java.sql.Timestamp;

public class GommetteEntity extends Entity
{
    private String nom;
    public  String getNom() { return nom; } 
    public  void   setNom(String value) { nom = value; }

    private Couleur couleur;
    private String strCouleur;

    public  Couleur getCouleur() { return couleur; } 
    public  void setCouleur(Couleur value) { 
        couleur = value; 
        strCouleur = value.getString();
    } 

    public String getStrCouleur() { return strCouleur; }

    private String description;
    public  String getDescription() { return description; } 
    public  void   setDescription(String value) { description = value; } 

    public GommetteEntity(){ }
}
