package com.uca;

import com.uca.dao._Initializer;
import com.uca.gui.*;
import com.uca.core.*;
import com.uca.entity.*;
import java.sql.Date;
import java.util.*;
import static spark.Spark.*;
import java.text.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import src.com.uca.helper.Hash;




public class StartServer {

    //.\gradlew run



    public static String SEL = "MangeMesCookies";
    
    public static boolean verificationSession(spark.Request req)
    {
        String c = req.cookie("session-id");

        if (c == null) return false;
        else{
            return true;
        }
    }

    public static void displayLocalHost(int portNb, String suffix)
    {
        System.out.println("http://localhost:"+portNb+"/"+suffix);
    }


    public static void main(String[] args) throws Exception{

        int portNb = 8081;
        //Configure Spark
        staticFiles.location("/static/");
        port(portNb);


        _Initializer.Init();

    
        //Defining our routes
        //displayLocalHost(portNb, "users");
        get("/users", (req, res) -> {
            return UserGUI.getAllUsers();
        });


        get("/createaccount", (req, res) -> {

            return NewAccountGUI.getNewAccountPage();
        });

        post("/createaccount", (req, res) -> {

            String firstName = req.queryParams("firstname");
            String lastName = req.queryParams("lastname");
            String username = req.queryParams("userName");
            String password = req.queryParams("password");
            
            Core.Prof.create(firstName, lastName, username, password);

            res.redirect("/login");

            return null;

        });

        displayLocalHost(portNb, "profs");
        get("/profs", (req, res) -> {
            if (verificationSession(req))
            {
                return ProfGUI.getAllProfs();
            }
            else
            {
                res.redirect("/login");
                return null;
            }
            
        });

        get("/welcome",(req,res) -> {
            if (verificationSession(req))
            {
                int id = Integer.parseInt(req.session().attribute("user-id").toString());
                ProfEntity connected = Core.Prof.getById(id);

                return ProfGUI.getWelcomePage(connected);
            }
            else
            {
                res.redirect("/login");
                return null;
            }
        });

        post("/profs/delete",(req,res) -> {
            Core.Prof.delete(Integer.parseInt(req.queryParams("id")));
            res.redirect("/profs");
            return null;
        });

    
        post("/logout", (req,res) -> {
            res.removeCookie("session-id");
            res.redirect("/login");
            return null;
        });


        displayLocalHost(portNb, "login");
        get("/login",(req,res)->{
            return LoginGUI.getConnectionPage();
        });

        post("/login",(req,res)->{
            String username = req.queryParams("userName");
            String password = req.queryParams("password");
            
            ProfEntity found = Core.Prof.getByLoginPair(username, password);
            
            if (found != null) {
                
                spark.Session s = req.session(true);
                s.attribute("user-id",Integer.toString(found.getId()));
                String id = s.id();
                res.cookie("session-id",id);
                res.redirect("/welcome");
                return null;
            }
                
            return "<!DOCTYPE html><html>Incorrect<a href=\'/login\'>Reessayer</a></html>";
        });


        get("/eleves",(req,res)->{
            if (verificationSession(req)) {
                return EleveGUI.getAllEleves();
            }
            else {
                res.redirect("/login");
                return null;
            }
        });

        get("/eleves/:id",(req,res)->{

            String id = req.params("id");
            return EleveGommetteGUI.getEleveGommettes(Integer.parseInt(id));
        });

        post("/eleves/ajout", (req,res)->{
            String firstName = req.queryParams("firstName");
            String lastName = req.queryParams("lastName");
            String dateNaissance = req.queryParams("dateNaissance");



            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date d = format.parse(dateNaissance);

            java.sql.Date date = new java.sql.Date(d.getTime());


            String nomClasse = req.queryParams("nomClasse");

            System.out.println(firstName + " " + lastName + " " + date.toString() + " " + nomClasse);

            Core.Eleve.create(firstName,lastName,date,nomClasse);

            res.redirect("/eleves");
            return null;
        });

        post("/eleves/gommette/delete", (req,res)->{
            System.out.println("deleted gommette");
            Core.GommetteAttrib.delete(Integer.parseInt(req.queryParams("id")));

            res.redirect("/eleves/"+req.queryParams("idEleve"));
            return null;
        });

        post("/eleves/gommette/ajout", (req,res)->{
            if (!verificationSession(req))
            {
                res.redirect("/login");
                return null;
            }



            String raison      = req.queryParams("raison");
            int idGommette     = Integer.parseInt(req.queryParams("idGommette"));
            int idEleve        = Integer.parseInt(req.queryParams("idEleve"));

            int id = Integer.parseInt(req.session().attribute("user-id").toString());
            ProfEntity connected = Core.Prof.getById(id);

            Core.GommetteAttrib.create(idEleve,connected.getId(),idGommette, new java.sql.Date(System.currentTimeMillis()), raison);
            res.redirect("/eleves/"+req.queryParams("idEleve"));
            return null;
        });

        post("/eleves/delete", (req,res) ->{
            Core.Eleve.delete(Integer.parseInt(req.queryParams("id")));

            res.redirect("/eleves");
            return null;
        });


        post("/eleves/update", (req,res) ->{

            String firstName = req.queryParams("firstName");
            String lastName = req.queryParams("lastName");
            String dateNaissance = req.queryParams("dateNaissance");

            String[] tokens = dateNaissance.split("-");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date d = format.parse(dateNaissance);

            java.sql.Date date = new java.sql.Date(d.getTime());
            

            String nomClasse = req.queryParams("nomClasse");
            String id = req.queryParams("id_selection");

            Core.Eleve.update(Integer.parseInt(id),firstName,lastName,date,nomClasse);
            res.redirect("/eleves");
            return null;
        });


        get("/gommettes", (req,res) ->{
            if (verificationSession(req)) {
                return GommetteGUI.getAllGommettes();
            }
            else {
                res.redirect("/login");
                return null;
            }
        });

        post("/gommettes/ajout", (req,res) ->{
            String couleur = req.queryParams("couleur");

            if (couleur != null)
            {
                Couleur c = Couleur.retrieve(couleur);
                System.out.println(c);
                if (c != null)
                { 
                    String description = req.queryParams("description");
                    String nom = req.queryParams("nom");
                    Core.Gommette.create(nom,c, description);
                    
                }
            }
            res.redirect("/gommettes");
            return null;
        });

        post("/gommettes/update", (req,res)->{

            
            String  couleurStr = req.queryParams("couleur");
            Couleur couleur = Couleur.retrieve(couleurStr);
        
            String id = req.queryParams("id_selection");
            String nom = req.queryParams("nom");
            String description = req.queryParams("description");

            GommetteEntity gommette = Core.Gommette.getById(Integer.parseInt(id));
            
            gommette.setNom(nom.length() > 0 ? nom : gommette.getNom());
            gommette.setCouleur(couleurStr != null ? couleur : gommette.getCouleur());
            gommette.setDescription(description.length() > 0 ? description : gommette.getDescription());
            
            Core.Gommette.push(gommette);
            
            res.redirect("/gommettes");
            return null;
        });



    }
}