package com.uca.dao;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.sql.*;

public class GommetteDAO extends _DefaultDAO<GommetteEntity> {

    @Override
    public String getDefaultTableName() { return "gommettes"; }

    @Override
    public void pushUnsafe(GommetteEntity obj) throws Exception
    {
        PreparedStatement stmt = this.connect.prepareStatement("update " + getDefaultTableName() + " set nom=?, description=?, couleur = ? where id=?;");
        int idx = 1;
        stmt.setString(idx++, obj.getNom());
        stmt.setString(idx++, obj.getDescription());
        stmt.setString(idx++, obj.getCouleur().getString());
        stmt.setInt   (idx++, obj.getId());
        stmt.executeUpdate() ;
    }

    @Override
    public GommetteEntity extractUnsafe(ResultSet resultSet) throws Exception
    {
        GommetteEntity entity = new GommetteEntity();
        entity.setId(resultSet.getInt("id"));
        entity.setCouleur(Couleur.retrieve(resultSet.getString("couleur")));
        entity.setNom(resultSet.getString("nom"));
        entity.setDescription(resultSet.getString("description"));
        return entity;
    }
}