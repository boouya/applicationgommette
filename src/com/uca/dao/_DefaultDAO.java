package com.uca.dao;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.util.*;
import java.sql.*;

public abstract class _DefaultDAO<T extends Entity> extends _Generic<T> {

    /**
     * Nom par défault de la table pour les opérations.
     * Null si ne peut être spécifier
     */
    public abstract String getDefaultTableName();

    /**
     * Sauvegarde une entrée dans la base de données
     */
    public T push(T obj)
    {
        try {
            pushUnsafe(obj);
        } catch (Exception e) {
            e.printStackTrace();
            obj = null;
        }
        return obj;
    }
    public abstract void pushUnsafe(T obj) throws Exception;

    /**
     * Extrait un objet depuis un de la table depuis le résultat d'une requête sql
     * Null si ne contient plus d'objets
     */
    public T extract(ResultSet resultSet)
    {
        try {
            if (resultSet.next()) 
            {
                return extractUnsafe(resultSet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public abstract T extractUnsafe(ResultSet resultSet) throws Exception;


    /**
     * Extrait tout les objets de la table
     */
    public List<T> getAll()
    {
        ArrayList<T> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM "+getDefaultTableName()+" ORDER BY id ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while(true)
            {
                T entity = extract(resultSet);
                if(entity == null) { return entities;}
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    @Override
    public T create(T obj)
    {
        try {
            PreparedStatement stmt = this.connect.prepareStatement("insert into "+getDefaultTableName()+" default values;", Statement.RETURN_GENERATED_KEYS);
            
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if(rs.next())
            {
                obj.setId(rs.getInt(1));
                return push(obj);
            }else { throw new Exception("no id"); }
        } catch (Exception e) {
            e.printStackTrace();
            obj = null;
        }
        return obj;
    }

    @Override
    public void delete(T obj) {
        if(obj != null)
        {
            delete(obj.getId());
        }
    }

    public T getById(int id)
    {
        T entity = null;
        try {
            PreparedStatement stmt = this.connect.prepareStatement("SELECT * FROM "+ getDefaultTableName() +" where id = ?;");
            stmt.setInt(1, id);
            return extract(stmt.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }
    
    public void delete(int id) {
        try {
            PreparedStatement stmt = this.connect.prepareStatement("DELETE FROM " + getDefaultTableName() +" WHERE id = ?;");
            stmt.setInt(1, id);
            stmt.executeUpdate() ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}