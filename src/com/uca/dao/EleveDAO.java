package com.uca.dao;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.sql.*;

public class EleveDAO extends _DefaultDAO<EleveEntity> {

    @Override
    public String getDefaultTableName() { return "eleves"; }

    @Override
    public void pushUnsafe(EleveEntity obj) throws Exception
    {
        PreparedStatement stmt = this.connect.prepareStatement("update " + getDefaultTableName() + " set firstName=?, lastName=?, nomClasse=?, dateNaissance=? where id=?;");
        int idx = 1;
        stmt.setString(idx++, obj.getFirstName());
        stmt.setString(idx++, obj.getLastName());
        stmt.setString(idx++, obj.getNomClasse());
        System.out.println(""+obj.getDateNaissance());
        stmt.setDate(idx++, obj.getDateNaissance());
        stmt.setInt   (idx++, obj.getId());
        stmt.executeUpdate();
    }

    @Override
    public EleveEntity extractUnsafe(ResultSet resultSet) throws Exception
    {
        EleveEntity entity = new EleveEntity();
        entity.setId(resultSet.getInt("id"));
        entity.setFirstName(resultSet.getString("firstname"));
        entity.setLastName (resultSet.getString("lastname"));
        entity.setDateNaissance(resultSet.getDate("dateNaissance"));
        entity.setNomClasse(resultSet.getString("nomClasse"));
        return entity;

    }
}