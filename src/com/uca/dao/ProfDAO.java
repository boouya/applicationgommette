package com.uca.dao;

import com.uca.*;
import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.sql.*;
import src.com.uca.helper.Hash;





public class ProfDAO extends _DefaultDAO<ProfEntity> {


    

    @Override
    public String getDefaultTableName() { return "profs"; }

    @Override
    public void pushUnsafe(ProfEntity obj) throws Exception
    {
        PreparedStatement stmt = this.connect.prepareStatement("update profs set firstName=?, lastName=?, userName=? , password=? where id=?;");
        int idx = 1;
        stmt.setString(idx++, obj.getFirstName());
        stmt.setString(idx++, obj.getLastName());
        stmt.setString(idx++, obj.getUserName());
        stmt.setString(idx++, obj.getPassword());
        stmt.setInt   (idx++, obj.getId());
        stmt.executeUpdate();
    }

    @Override
    public ProfEntity extractUnsafe(ResultSet resultSet) throws Exception
    {
        ProfEntity entity = new ProfEntity();
        entity.setId(resultSet.getInt("id"));
        entity.setFirstName(resultSet.getString("firstName"));
        entity.setLastName(resultSet.getString("lastName"));
        entity.setUserName(resultSet.getString("userName"));
        entity.setPassword(resultSet.getString("password"));

        return entity;
    }
}