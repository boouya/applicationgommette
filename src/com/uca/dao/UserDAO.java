package com.uca.dao;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.sql.*;

public class UserDAO extends _DefaultDAO<UserEntity> {

    @Override
    public String getDefaultTableName() { return "users"; }

    @Override
    public void pushUnsafe(UserEntity obj) throws Exception
    {
        PreparedStatement stmt = this.connect.prepareStatement("update "+ getDefaultTableName() +" set firstName=?, lastName=? where id=?;");
        int idx = 1;
        stmt.setString(idx++, obj.getFirstName());
        stmt.setString(idx++, obj.getLastName());
        stmt.setInt   (idx++, obj.getId());
        stmt.executeUpdate();
    }

    @Override
    public UserEntity extractUnsafe(ResultSet resultSet) throws Exception
    {
        UserEntity entity = new UserEntity();
        entity.setId(resultSet.getInt("id"));
        entity.setFirstName(resultSet.getString("firstname"));
        entity.setLastName(resultSet.getString("lastname"));
        return entity;
    }
}
