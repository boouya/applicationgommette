package com.uca.dao;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.sql.*;
import java.lang.Math;
public class _Initializer {

    public static ProfEntity DEBUG_BORIS_ADMIN;
    public static ProfEntity DEBUG_TOTO_ADMIN;

    public static void DropTable(String name)
    {
        try {
            _Connector.getInstance().prepareStatement("DROP TABLE "+name+";").executeUpdate();
        } catch (Exception e){
            System.out.println(e.toString());
            throw new RuntimeException("could not drop table: "+name);
        }
    }

    public static void CreateTable(String command)
    {
        //System.out.println("Creation de la table "+command.split(" ")[0]);
        System.out.println(command);

        command = "CREATE TABLE IF NOT EXISTS "+command;
        try {
            PreparedStatement statement;

            //Init articles table
            _Connector.getInstance().prepareStatement(command).executeUpdate();
        } catch (Exception e){
            System.out.println(e.toString());
            throw new RuntimeException("could not create database: "+command);
        }
    }

    private static int rng(int min, int max)
    {
        return (int)(Math.random()*(max-min+1)+min);
    }

    private static void maybeAdd(EleveEntity e, GommetteEntity g, String motif)
    {
        if(rng(0,100) < 60)
        {
            Core.GommetteAttrib.create(e.getId(),
            (rng(0, 100) >= 50 ? DEBUG_BORIS_ADMIN : DEBUG_TOTO_ADMIN).getId(),
            g.getId(), new java.sql.Date(44938), motif);
        }
    }

    private static java.sql.Date createSqlDate(int day, int month, int year) {

        // Create a date string with leading zeros for month and day.
        String dateString = String.format("%d-%02d-%02d", year, month, day);
    
        return java.sql.Date.valueOf(dateString);
    }

    private static EleveEntity createEleve(String nom, String prenom)
    {
        EleveEntity e = Core.Eleve.create(nom, prenom,createSqlDate(rng(1, 27),rng(1, 11), rng(2000, 2010)),"Classe 1");
        maybeAdd(e, bruyant, "Je vais finir par t'isoler");
        maybeAdd(e, uniCorn, "J'espère que tu continueras à arborer ces couleurs arc en ciel plus tard");
        maybeAdd(e, sagePetit, "Hehe boi");
        return e;
    }

    private static GommetteEntity bruyant;
    private static GommetteEntity uniCorn;
    private static GommetteEntity sagePetit;

    public static void Init(){

        try
        {
            DropTable("users");
            DropTable("profs");
            DropTable("eleves");
            DropTable("gommettes");
            DropTable("gommetteAttribs");
    
            CreateTable("users (id int primary key auto_increment, firstname varchar(100), lastname varchar(100));");
            CreateTable("profs (id int primary key auto_increment, firstname varchar(100), lastname varchar(100), username  varchar(100), password varchar(100));");
            CreateTable("eleves (id int primary key auto_increment, firstname varchar(100), lastname varchar(100), nomClasse varchar(100), dateNaissance DATE);");
            CreateTable("gommettes (id int primary key auto_increment, nom varchar(100), description varchar(100), couleur varchar(100));");
            CreateTable("gommetteAttribs (id int primary key auto_increment, idEleve int, idProf int, idGommette int, dateAttribution DATE, raison varchar(500));");
            
            sagePetit = Core.Gommette.create("Brave Petit",    Couleur.Vert,      "Citoyen d'honneur");
            uniCorn   = Core.Gommette.create("Badge Unicorne", Couleur.ArcEnCiel, "C'est magique!");
            bruyant   = Core.Gommette.create("Bruyant",        Couleur.Rouge,     "Bavardage!!!");
    
            DEBUG_BORIS_ADMIN = Core.Prof.create("Boris", "OUYA", "DaWarudo","KonoDioDa");
            DEBUG_TOTO_ADMIN  = Core.Prof.create("Thomas", "TAMAGNAUD", "CéMoiLul","BlockusMaster");
    
            Core.GommetteAttrib.create(createEleve("Sacha", "Touille").getId(), DEBUG_BORIS_ADMIN.getId(), Core.Gommette.create("Chasseur", Couleur.Rouge, "passe son temps à attraper des pokemons").getId(), new java.sql.Date(44938), "Arrete de racketter tes camarades");
            Core.GommetteAttrib.create(createEleve("Paul", "Hisse").getId(), DEBUG_TOTO_ADMIN.getId(), Core.Gommette.create("Autoritaire", Couleur.Rouge, "Fait peur à ses camarades").getId(), new java.sql.Date(44938), "La force de l'ordre ici c'est moi.");
            Core.GommetteAttrib.create(createEleve("Terry", "Golo").getId(), DEBUG_BORIS_ADMIN.getId(), Core.Gommette.create("Blagueur", Couleur.ArcEnCiel, "Fait des blagues").getId(), new java.sql.Date(44938), "Faudrai songer à changer de disque.");
            
            createEleve("Kelly", "Diote");
            createEleve("Matt",  "Lecul");
            createEleve("Lorie", "Fice");
            createEleve("Judas", "Nanas");
    
            Core.Prof.create("Jean", "Pierre", "Framboisier","Fraise");
            Core.Prof.create("Melina", "Myr", "Myrtille ","ksix");
    
    
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        

        /*
        EleveEntity bibi = Core.Eleve.create("Bibi","Johnson",new java.sql.Date(44938),"Zarbi");
        EleveEntity bobo = Core.Eleve.create("Bobo","Bis",new java.sql.Date(10),"Pokemon");

        Core.GommetteAttrib.create(bibi.getId(), DEBUG_BORIS_ADMIN.getId(), sagePetit.getId(), new java.sql.Date(44938), "Aucune raison");
        Core.GommetteAttrib.create(bibi.getId(), toto.getId(), uniCorn.getId(), new java.sql.Date(44938), "Our Little Poney");

        Core.GommetteAttrib.create(bobo.getId(), DEBUG_BORIS_ADMIN.getId(), uniCorn.getId(), new java.sql.Date(32), "Bravo bobo");*/

        
    }
}
