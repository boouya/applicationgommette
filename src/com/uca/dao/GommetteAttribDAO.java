package com.uca.dao;

import com.uca.core.*;
import com.uca.dao.*;
import com.uca.entity.*;
import java.sql.*;

public class GommetteAttribDAO extends _DefaultDAO<GommetteAttribEntity> {

    @Override
    public String getDefaultTableName() { return "gommetteAttribs"; }

    @Override
    public void pushUnsafe(GommetteAttribEntity obj) throws Exception
    {
        PreparedStatement stmt = this.connect.prepareStatement("update " + getDefaultTableName() + " set idEleve=?, idProf=?, idGommette=?, raison=?, dateAttribution=? where id=?;");
        int idx = 1;
        stmt.setInt   (idx++, obj.getIdEleve());
        stmt.setInt   (idx++, obj.getIdProf());
        stmt.setInt   (idx++, obj.getIdGommette());
        stmt.setString(idx++, obj.getRaisonAttribution());
        stmt.setDate(idx++, obj.getDateAttribution());
        stmt.setInt   (idx++, obj.getId());
        stmt.executeUpdate();
    }

    @Override
    public GommetteAttribEntity extractUnsafe(ResultSet resultSet) throws Exception
    {
        GommetteAttribEntity entity = new GommetteAttribEntity();
        entity.setId(resultSet.getInt("id"));
        entity.setIdEleve(resultSet.getInt("idEleve"));
        entity.setIdProf(resultSet.getInt("idProf"));
        entity.setIdGommette(resultSet.getInt("idGommette"));
        entity.setRaisonAttribution(resultSet.getString("raison"));
        entity.setDateAttribution(resultSet.getDate("dateAttribution"));
        return entity;
    }
}